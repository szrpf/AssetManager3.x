import * as cc from 'cc';
const { ccclass } = cc._decorator;

@ccclass('分步加载分包')
export class 分步加载分包 extends cc.Component {

    start() {
        gi.setChannel('LLK');
        this.演示如何加载分包();
        this.演示如何同步换图换Spine();
        this.演示如何调用其他子游戏的资源();
    }

    演示如何加载分包() {
        //进入游戏后，开始加载分包
        gi.loadBundle('SubPack', (progress, path, asset) => {
            //每加载一个资源触发一次回调
            switch (path) {
                //Bgm加载好了，开始播放Bgm
                case 'SubPack/Bgm':
                    this.node.getComponent(cc.AudioSource).clip = asset;
                    this.node.getComponent(cc.AudioSource).play();
                    break;
                //单张图片“地球”加载好了，在Sprite上显示出来
                case 'SubPack/地球':
                    this.node.getChildByName('Sprite').getComponent(cc.Sprite).spriteFrame = asset;
                    break;
                //Spine数据加载好了，显示出来并设置初始动画
                case 'SubPack/Avatar/avatar_ox':
                    this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).skeletonData = asset;
                    this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).setAnimation(0, 'win2', true);
                    break;
                //粒子数据加载好了，则显示出来
                case 'SubPack/Particle/xunlongduobao_datingdonghua_lizi':
                    this.node.getChildByName('Particle').getComponent(cc.ParticleSystem2D).file = asset;
                    break;
                //预制体加载好了，则实例化并挂到Canvas下显示出来
                case 'SubPack/Hero':
                    let node = cc.instantiate(asset);
                    node.setParent(this.node);
                    node.setPosition(cc.v3(-200, 500, 0));
                    break;
            }
        });
    }

    演示如何同步换图换Spine() {
        //点击图片，同步换图
        this.node.getChildByName('Sprite').on(cc.NodeEventType.TOUCH_START, () => {
            this.node.getChildByName('Sprite').getComponent(cc.Sprite).spriteFrame = gi.load('MainPack/Image/开战');
        }, this);
        //松开图片，同步恢复
        this.node.getChildByName('Sprite').on(cc.NodeEventType.TOUCH_END, () => {
            this.node.getChildByName('Sprite').getComponent(cc.Sprite).spriteFrame = gi.load('SubPack/地球');
        }, this);
        //点击Spine动画，同步换Spine
        this.node.getChildByName('Spine').on(cc.NodeEventType.TOUCH_START, () => {
            this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).skeletonData = gi.load('MainPack/tuzi/tuzi');
            this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).setAnimation(0, 'animation2', true);
        }, this);
        //点击图片，同步恢复
        this.node.getChildByName('Spine').on(cc.NodeEventType.TOUCH_END, () => {
            this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).skeletonData = gi.load('SubPack/Avatar/avatar_ox');
            this.node.getChildByName('Spine').getComponent(cc.sp.Skeleton).setAnimation(0, 'win2', true);
        }, this);
    }

    演示如何调用其他子游戏的资源(){
        (async ()=>{
            //切换子游戏
            gi.setChannel('XXL');
            //使用gi.loadAsync异步加载图片、Spine
            this.node.getChildByName('Sprite1').getComponent(cc.Sprite).spriteFrame = await gi.loadAsync('MainPack/树');
            this.node.getChildByName('Spine1').getComponent(cc.sp.Skeleton).skeletonData = await gi.loadAsync('SubPack/铁头');
            this.node.getChildByName('Spine1').getComponent(cc.sp.Skeleton).setAnimation(0, '攻击3', true);
            //切换子游戏
            gi.setChannel('LLK');
        })();
    }
}