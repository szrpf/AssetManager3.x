import * as cc from 'cc';
const { ccclass } = cc._decorator;

@ccclass('Loading')
export class Loading extends cc.Component {
    start() {
        gi.setChannel('LLK');
        gi.loadBundle('MainPack', (progress, path, asset) => {
            //每加载一个资源触发一次回调，根据progress更新加载进度条
            this.node.getChildByName('ProgressBar').getComponent(cc.ProgressBar).progress = progress;
            gi.alert('加载进度：', progress.toFixed(2), '当前加载的资源：', path);
            if (progress === 1) {
                gi.alert('Loading完毕，进入游戏！');
                cc.director.loadScene('Game');
            }
        });
    }
}