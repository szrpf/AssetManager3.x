import * as cc from 'cc';
import { ChannelOf } from '../gi';
const { ccclass } = cc._decorator;

@ccclass('多语言演示')
export class 多语言演示 extends cc.Component {

    start() {
        //切换到XXL子游戏
        gi.setChannel('XXL');
        //读取MainPack
        gi.loadBundle('MainPack', (progress, path, asset) => {
            //读取完毕后，初始化ZH和EN两个语种的数据
            if (progress === 1) {
                gi.langLoad('ZH', gi.load('MainPack/LangZH'));
                gi.langLoad('EN', gi.load('MainPack/LangEN'));
            }
        })
        //点击按钮，切换到ZH语种，包括所有文字、图片
        this.node.getChildByName('ZH').on(cc.NodeEventType.TOUCH_START, () => {
            gi.setChannel('ZH', ChannelOf.Language);
        }, this);
        //点击按钮，切换到EN语种，包括所有文字、图片
        this.node.getChildByName('EN').on(cc.NodeEventType.TOUCH_START, () => {
            gi.setChannel('EN', ChannelOf.Language);
        }, this);
    }
}